import sass from 'rollup-plugin-sass';
import copy from 'rollup-plugin-copy';
import typescript from 'rollup-plugin-typescript2';
import svgr from '@svgr/rollup';

import pkg from './package.json';

export default {
  input: 'src/index.tsx',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: 'inline',
      strict: false,
    },
  ],
  plugins: [
    copy({
      targets: [
        { src: 'src/fonts/**/*', dest: 'dist/fonts' },
        { src: 'src/_*.scss', dest: 'dist/scss' },
      ],
    }),
    svgr(),
    sass({ insert: true }),
    typescript(),
  ],
  external: ['react', 'react-dom', 'lodash/isEmpty', 'classnames'],
};
