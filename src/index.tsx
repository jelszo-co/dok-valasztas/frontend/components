import * as React from 'react';
import './styles.scss';
import isEmpty from 'lodash/isEmpty';
import classNames from 'classnames';

// import Clip from './candidateCardClip.svg';

export enum Colors {
  Bright = 'color-bright',
  Dark = 'color-dark',
  Gradient = 'color-gradient',
}

export const SectionHeading: React.FC<{ color: Colors }> = ({ color, children }) => (
  <h2 className={classNames('section-heading', color)}>{children}</h2>
);

export const GeneralCTA: React.FC<{
  color: Colors;
  bgColor: Colors;
  callback: Function;
  className?: Object;
}> = ({ color, bgColor, callback, children, className }) => (
  <button
    className={classNames('general-cta', color, 'bg-' + bgColor, className)}
    onClick={() => callback()}
  >
    {children}
  </button>
);

export interface Candidate {
  id: number;
  name: string[];
  description?: string;
  picture?: string;
}

const pattern = (base64Image: string, uid: number) => (
  <>
    <pattern
      id={`candidate-image-background-${uid}`}
      patternContentUnits='objectBoundingBox'
      width='1'
      height='1'
    >
      <use
        xlinkHref={`#candidate-image-${uid}`}
        transform='translate(-0.258924) scale(0.0010119 0.00143062)'
      />
    </pattern>
    <image
      id={`candidate-image-${uid}`}
      xlinkHref={`data:image/png;base64,${base64Image}`}
      x='0'
      y='0'
      width='1500'
      height='699'
    />
  </>
);

export const Image: React.FC<{ base64Image: string; uid: number }> = ({ base64Image, uid }) => {
  return (
    <svg
      viewBox='0 0 367 260'
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink '
    >
      <defs>{pattern(base64Image, uid)}</defs>
      <path
        d='M53.3991 187.782C11.9836 187.782 0 159.144 0 124.567V-1H369V260C369 204.294 334.775 187.782 297.098 187.782H53.3991Z'
        fill={`url(#candidate-image-background-${uid})`}
      />
    </svg>
  );
};
export const CandidateCard: React.FC<{ c: Candidate; click: Function }> = ({ c, click }) => {
  const hasImage = !isEmpty(c.picture);
  return (
    <div className={classNames('candidate-card', { 'candidate-card-image': hasImage })}>
      <div className='candidate-card-content'>
        {hasImage && <Image base64Image={c.picture!} uid={c.id} />}
        {c.name.map(name => (
          <h3 className='candidate-name' key={name}>
            {name}
          </h3>
        ))}

        <p>{c.description}</p>
        <GeneralCTA
          color={Colors.Bright}
          bgColor={Colors.Gradient}
          callback={() => click()}
          className='candidate-cta'
        >
          Tovább olvasok!
        </GeneralCTA>
      </div>
      <div className='candidate-card-hover-border' />
      <div className='candidate-card-hover-shadow' />
    </div>
  );
};

const candidateSelectCardText = (c: Candidate, selected: boolean): string => {
  const plural = c.name.length > 1;
  if (selected) {
    if (plural) return 'Őket választom!';
    else return 'Őt választom!';
  } else {
    if (plural) return 'Rájuk szavazok!';
    else return 'Rá szavazok!';
  }
};

export const CandidateCardSelect: React.FC<{
  c: Candidate;
  selected: boolean;
  click: Function;
}> = ({ c, selected, click }) => {
  const hasImage = !isEmpty(c.picture);
  return (
    <div
      className={classNames('candidate-card-select', { 'candidate-card-select-current': selected })}
    >
      <div className='candidate-card-select-content'>
        {hasImage && <Image base64Image={c.picture!} uid={c.id} />}
        {c.name.map(name => (
          <h3 className='candidate-name' key={name}>
            {name}
          </h3>
        ))}
        <GeneralCTA
          color={Colors.Bright}
          bgColor={Colors.Gradient}
          callback={() => click()}
          className='candidate-select-cta'
        >
          {candidateSelectCardText(c, selected)}
        </GeneralCTA>
      </div>
      <div className='candidate-card-select-border' />
      <div className='candidate-card-select-shadow' />
    </div>
  );
};

export const CandidateCardOverview: React.FC<{
  c: Candidate;
}> = ({ c, children }) => {
  const hasImage = !isEmpty(c.picture);
  return (
    <div className='candidate-card-overview'>
      {hasImage && <Image base64Image={c.picture!} uid={c.id} />}
      {c.name.map(name => (
        <h3 className='candidate-name' key={name}>
          {name}
        </h3>
      ))}
      <p>{children}</p>
    </div>
  );
};
